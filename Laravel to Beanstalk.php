<!-- 1. Creating Laravel -->
Step 1: composer create-project laravel/laravel "name"


<!-- 2. Uploading to Gitlab -->
Step 1: Create repo in Github
Step 2: Push your created laravel to your repo by:
Step 3: git init
Step 4: git remote add origin [HTTP]
Step 5: git add .
Step 6: git commit -m "comment"
Step 7: git push origin master


<!-- 3. Creating Beanstalk -->
Step 1: Click "Create Application"
Step 2: Add application name
Step 3: Select "PHP" (Platform)
Step 4: Click "Create application"
Step 5: *** After installation
Step 6: Go to configuration
Step 7: Edit (Software)
Step 8: Select Apache (Proxy server)
Step 9: /public (Document root)
Step 10: Input APP_KEY from .env (Environment properties)
Step 11: Apply
 

<!-- 4. Uploading to Code Pipeline -->
Step 1: Create pipeline
Step 2: Add pipeline name
Step 3: Set all Default (Advance settings)
Step 4: NEXT
Step 5: Select Github(Version 1)
Step 6: Click connect to GitHub
Step 7: Select your created repository from GitHub (Repo)
Step 8: Select master (Branch)
Step 9: NEXT
Step 10: SKIP BUILD STAGE
Step 11: Select "AWS Elastic Beanstalk" (Deploy)
Step 12: Select Region
Step 13: Select Application name
Step 14: NEXT
Step 15: Create pipeline


